from setuptools import setup
setup(
    name='test-hello',
    version='1.0',
    description='test',
    url='https://git.glebmail.xyz/',
    author='gleb',
    packages=['test-hello'],
    author_email='gleb@glebmail.xyz',
    license='GNU GPL 3',
    scripts=['bin/test-hello','bin/test-hello-run']
)

