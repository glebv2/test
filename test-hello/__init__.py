import sys

def run():
    test = 'hello world!'
    print(test)
    return test

def unit():
    status = run()
    if status == 'hello world!':
       print('unit tested')
       sys.exit(0)
    else:
       print('test failed!')
       sys.exit(1)

try:
   if sys.argv[1] == 'test':
      unit()
except IndexError:
   run()
